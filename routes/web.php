<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});*/





Route::prefix('backend')->group(function () {

    Route::get('sitepage', 'PageController@index')->middleware('auth');
    Route::get('/', 'EventController@index')->middleware('auth');

    

    Route::prefix('events')->group(function () {
        Route::get('/', 'EventController@index')->middleware('auth');
        Route::get('create', 'EventController@create')->middleware('auth');
        Route::get('edit/{id}', 'EventController@edit')->middleware('auth');
        Route::get('delete/{id}', 'EventController@delete')->middleware('auth');
        Route::post('save', 'EventController@save')->middleware('auth');
        Route::post('uploadImgContent', 'EventController@uploadImgContent')->middleware('auth');
        Route::get('eventRegister/{refEventID}', 'EventController@eventRegister')->middleware('auth');
        Route::get('eventRegister/{refEventID}/export', 'EventController@eventRegisterExport')->middleware('auth');
    });


    Route::prefix('pages')->group(function () {
        Route::get('/', 'PageController@index')->middleware('auth');
        Route::get('', 'PageController@index')->middleware('auth');
        Route::get('create', 'PageController@create')->middleware('auth');
        Route::get('edit/{id}', 'PageController@edit')->middleware('auth');
        Route::get('delete/{id}', 'PageController@delete')->middleware('auth');
        Route::post('save', 'PageController@save')->middleware('auth');
        Route::post('uploadImgContent', 'PageController@uploadImgContent')->middleware('auth');
    });


    Route::prefix('menus')->group(function () {
        Route::get('/', 'MenuController@index')->middleware('auth');
        Route::get('create', 'MenuController@create')->middleware('auth');
        Route::get('edit/{id}', 'MenuController@edit')->middleware('auth');
        Route::get('delete/{id}', 'MenuController@delete')->middleware('auth');
        Route::post('updateposition', 'MenuController@updateposition')->middleware('auth');
        Route::post('save', 'MenuController@save')->middleware('auth');
      
    });

});


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/view/{codepage}', 'HomeController@viewpage');
Route::get('/events', 'HomeController@events');
Route::get('/events/{id}', 'HomeController@event');
Route::get('/register/{id}', 'RegisterController@index');
Route::get('/registerResult/{id}', 'RegisterController@result');
Route::prefix('application')->group(function () {
        Route::post('save', 'RegisterController@save');
});

Route::get('/home', 'HomeController@index')->name('home');
