<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 200);
            $table->longText('description')->nullable($value = true);
            $table->float('lat', 8, 6)->nullable($value = true);
            $table->float('lng', 8, 6)->nullable($value = true);
            $table->string('keyword')->nullable($value = true);
            $table->string('coverimage')->nullable($value = true);
            $table->date('from')->nullable($value = true);
            $table->date('to')->nullable($value = true);
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
