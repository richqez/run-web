@extends('frontend.layout.master')

@section('content')
  <!-- start banner Area -->
  <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>	
        <div class="container">
            <div class="row fullscreen d-flex align-items-center justify-content-between">
                <div class="banner-content col-lg-6 col-md-12 ">
                    <h1 class="text-uppercase">
                            กิจกรรม เดิน-วิ่ง การกุศล วัดดงมณี                                     
                    </h1>
                    <p class="pt-10 pb-10 text-white">
                        <!-- Tremblant is based in Canada and has over 90 runs servicing millions of skiers each year. With 13 state-of-the-art ski lifts and a selection of choices for both snowboarders and skiers. -->
                    </p>
                    <a href="#" class="primary-btn">กิจกรรมล่าสุดของเรา</a>
                </div>										
            </div>
        </div>					
    </section>
    <!-- End banner Area -->	

    <!-- Start top-course Area -->
    <section class="top-course-area section-gap">
        <div class="container">
            <div class="row section-title">
                <h1>กิจกรรม</h1>
                <p></p>
            </div>	
            <div class="row">
                <div class="active-topcourse-carusel">
                    @foreach ($events as $event)
                        <div class="single-carusel item">
                            <div class="thumb">
                                <img style="height:216px" class="img-fluid" src="{{ url('/public/event_photo/' . $event->coverimage )  }}" alt="">
                                <div class="join-btn"><a href="{{ url('/events/' . $event->id )}}">เพิ่มเติม</a></div>
                            </div>
                            <div class="title-price d-flex justify-content-between">
                                <a href="{{ url('/events/' . $event->id )}}">
                                    <h4>{{$event->title}}</h4>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    
																												
                </div>
            </div>
        </div>	
    </section>
    <!-- End top-course Area -->
    
    <!-- Start cta Area -->
    {{-- <section class="cta-area">
        <div class="container-fluid">
            <div class="row no-padding">
                <div class="col-lg-6 single-cta cta1 no-padding section-gap relative">
                    <div class="overlay overlay-bg"></div>
                    <h6 class=text-uppercase>ข่าวสาร</h6>
                    <h1></h1>
                    <a href="#" class="primary-btn">เพิ่มเติ่ม</a>
                </div>
                <div class="col-lg-6 single-cta cta2 no-padding section-gap relative">
                    <div class="overlay overlay-bg"></div>
                    <h6 class=text-uppercase>เว็บบอร์ด</h6>
                    <h1></h1>
                    <a href="#" class="primary-btn">เว็บบอร์ด</a>			

                </div>
            </div>
        </div>	
    </section> --}}
    <!-- End cta Area -->
    
    <!-- Start feature Area -->
    {{-- <section class="feature-area section-gap">
        <div class="container">
            <div class="row section-title">
                <h1>การเตรียมร่างกาย</h1>
                <p></p>
            </div>						
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 feature-left">
                    <img class="img-fluid" src="{{ url('/') }}/public/fitness/img/f.jpg" alt="">
                </div>
                <div class="col-lg-6 feature-right">
                    <div class="single-feature">
                        <h4>การเตรียมร่างกาย</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                    <div class="single-feature">
                        <h4>การเตรียมร่างกาย</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>	
                    <div class="single-feature">
                        <h4>การเตรียมร่างกาย</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.	
                        </p>
                    </div>														
                </div>
            </div>
        </div>	
    </section> --}}
    <!-- End feature Area -->
    
    <!-- Start calculation Area -->
    {{-- <section class="aclculation-area section-gap relative">
        <div class="overlay overlay-bg"></div>				
        <div class="container">
            <div class="row section-title relative">
                <h1 class="text-white">ระบบคำนวณความเร็วและฝีเท้า</h1>
            </div>					
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-2 title-row">
                    <p class="text-white">ระยะทาง</p>
                </div>
                <div class="col-lg-5">
                    <input type="text" class="form-control" name="height">
                </div>
                <div class="col-lg-1">
                    <span class="text-white">เมตร</span>
                </div>					
            </div>
            <div class="row justify-content-center align-items-center pt-30">
                <div class="col-lg-2 title-row">
                    <p class="text-white">เวลา</p>
                </div>
                <div class="col-lg-1">
                    <input type="text" class="form-control" name="height" placeholder="">
                </div>
                <div class="col-lg-1">
                    <span class="text-white">ชม.</span>
                </div>	
                <div class="col-lg-1">
                    <input type="text" class="form-control" name="height" placeholder="">
                </div>
                <div class="col-lg-1">
                    <span class="text-white">นาที</span>
                </div>	
                <div class="col-lg-1">
                    <input type="text" class="form-control" name="height" placeholder="">
                </div>
                <div class="col-lg-1">
                    <span class="text-white">วินาที</span>
                </div>	
                                        
            </div>
            <div class="row justify-content-center align-items-center pt-30">
                <div class="col-lg-2 title-row">
                    
                </div>
                <div class="col-lg-5">
                    <a href="#" class="primary-btn">คำนวน</a>
                </div>	
                <div class="col-lg-1">
                </div>	
            </div>
        </div>	
    </section> --}}
    <!-- End calculation Area -->
    
    <!-- Start image-gallery Area -->
    <section class="image-gallery-area section-gap" style="background-color: #f9f9ff">
        <div class="container">
            <div class="row section-title">
                <h1>ร่วมภาพกิจกรรม</h1>
                <p></p>
            </div>					
            <div class="row">
                @foreach($home_image as $chunk)
                    <div class="col-lg-4 single-gallery">
                        @foreach($chunk as $c)
                            @if(isset($c) && $c != null)
                    <a href="{{$c}}"   class="img-gal"><img
                        class="img-fluid" src="{{$c}}" alt="{{$loop->index +1}}"></a>
                            @endif
                        @endforeach
                    </div>
                @endforeach

                {{-- <div class="col-lg-4 single-gallery">
                    <a href="{{ url('/') }}/public/fitness/img/g1-2.jpg" class="img-gal"><img class="img-fluid" src="{{ url('/') }}/public/fitness/img/g1-2.jpg" alt=""></a>
                    <a href="{{ url('/') }}/public/fitness/img/g4.jpg" class="img-gal"><img class="img-fluid" src="{{ url('/') }}/public/fitness/img/g4-2.jpg"  style="height: 485px !important;" alt=""></a>
                </div>	
                <div class="col-lg-4 single-gallery">
                    <a href="{{ url('/') }}/public/fitness/img/g2.jpg" class="img-gal"><img class="img-fluid" src="{{ url('/') }}/public/fitness/img/g2-2.jpg" style="width: 85% !important;" alt=""></a>
                    <a href="{{ url('/') }}/public/fitness/img/g5.jpg" class="img-gal"><img class="img-fluid" src="{{ url('/') }}/public/fitness/img/g5.jpg" alt=""></a>						
                </div>	
                <div class="col-lg-4 single-gallery">
                    <a href="{{ url('/') }}/public/fitness/img/g3.jpg" class="img-gal"><img class="img-fluid" src="{{ url('/') }}/public/fitness/img/g3.jpg" alt=""></a>
                    <a href="{{ url('/') }}/public/fitness/img/g6.jpg" class="img-gal"><img class="img-fluid" src="{{ url('/') }}/public/fitness/img/g6.jpg" alt=""></a>
                </div>				 --}}
            </div>
        </div>	
    </section>
    <!-- End image-gallery Area -->	

    <!-- Start callto Area -->
    {{-- <section class="callto-area section-gap relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row">
                <div class="call-wrap mx-auto">
                    <h1>เข้าร่วมกับเรา</h1>
                    <p>
                    </p>
                    <a href="#" class="primary-btn">สมัครสมาชิก</a>				
                </div>
            </div>
        </div>	
    </section> --}}
    <!-- End callto Area -->
@endsection