
@extends('frontend.layout.master')
@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
        <div class="overlay overlay-bg"></div>
        <div class="container">				
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                     {{ $event->title }}			
                    </h1>	
                </div>	
            </div>
        </div>
    </section>
    <!-- End banner Area -->					  
    
    <!-- Start post-content Area -->
    <section class="post-content-area single-post-area" style="padding-top:0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-12">
                            <div class="feature-img">
                                <img class="img-fluid" src="img/blog/feature-img1.jpg" alt="">

                            </div>									
                        </div>
                        </div>
                        
                        <div class="col-lg-12 col-md-12 TextEditorImg">
                             <form action="{{ url('/application/save') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="refEventID" value="{{ $event->id }}">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">ชื่อ</label>
                                    <div class="col-sm-4">
                                      <input type="text" name="firstName" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">นามสกุล</label>
                                    <div class="col-sm-4">
                                      <input type="text" name="lastName" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">ชมรม</label>
                                    <div class="col-sm-4">
                                      <input type="text" name="clubName" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">ที่อยู่</label>
                                    <div class="col-sm-4">
                                      <textarea name="address" class="form-control" rows="3" required></textarea>
                                    </div>
                                     <label for="inputEmail3" class="col-sm-2 col-form-label">รหัสไปรษณีย์</label>
                                    <div class="col-sm-4">
                                     <input type="text" name="zipCode" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">เบอร์โทรศัพท์</label>
                                    <div class="col-sm-4">
                                     <input type="text" name="tel" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                     <label for="inputEmail3" class="col-sm-2 col-form-label">ไซส์</label>
                                    <div class="col-sm-4">
                                     <input type="text" name="size" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">BIB</label>
                                    <div class="col-sm-4">
                                     <input type="text" name="bib" class="form-control" id="inputEmail3" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group row text-center">
                                    <label for="inputEmail3" class="col-sm-12 col-form-label"><h2>ประเภทการแข่งขัน</h2></label>
                                </div>
                                <div class="form-group row text-center">
                                    <label for="inputEmail3" class="col-4 col-form-label">21 k.(450 บาท) <br>05.00 น.</label>
                                    <label for="inputEmail3" class="col-4 col-form-label">10.5 k.(400 บาท) <br>05.00 น.</label>
                                    <label for="inputEmail3" class="col-4 col-form-label">5 k.(400 บาท) <br>05.00 น.</label>
                                </div>
                                <div class="form-group row text-center">
                                    <label for="inputEmail3" class="col-2 col-form-label">ชาย</label>

                                    <label for="inputEmail3" class="col-2 col-form-label">หญิง</label>
                                    <label for="inputEmail3" class="col-2 col-form-label">ชาย</label>
                                    <label for="inputEmail3" class="col-2 col-form-label">หญิง</label>
                                    <label for="inputEmail3" class="col-2 col-form-label">ชาย</label>
                                    <label for="inputEmail3" class="col-2 col-form-label">หญิง</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2">
                                        <div class="radio">
                                            <label><input type="radio" value="21km16-19ชาย" name="registerType" checked>  16 - 19</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio"  value="21km20-24ชาย" name="registerType">  20 - 24</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km25-29ชาย" name="registerType">  25 - 29</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km30-34ชาย" name="registerType">  30 - 34</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km35-39ชาย" name="registerType">  35-39</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km40-44ชาย" name="registerType">  40 - 44</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km45-49ชาย" name="registerType">  45 - 49</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km50-54ชาย" name="registerType">  50 - 54</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km55-59ชาย" name="registerType">  55 - 59</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km16-19ชาย" name="registerType">  60 - 64</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km65-69ชาย" name="registerType">  65 - 69</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km70...ชาย" name="registerType">  70...</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="radio">
                                            <label><input type="radio" value="21km16-19หญิง" name="registerType">  16 - 19</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio"  value="21km20-24" name="registerType">  20 - 24</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km25-29หญิง" name="registerType">  25 - 29</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km30-34หญิง" name="registerType">  30 - 34</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km35-39หญิง" name="registerType">  35-39</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km40-44หญิง" name="registerType">  40 - 44</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km45-49หญิง" name="registerType">  45 - 49</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km50-54หญิง" name="registerType">  50 - 54</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km55-59หญิง" name="registerType">  55 - 59</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="21km60...หญิง" name="registerType">  60...</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="radio">
                                            <label><input type="radio" value="ไม่เกิน 15 ปีชาย" name="registerType">  ไม่เกิน 15 ปี</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km16-19ชาย" name="registerType">  16 - 19</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km20-24ชาย" name="registerType">  20 - 24</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km25-29ชาย" name="registerType">  25 - 29</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km30-34ชาย" name="registerType">  30 - 34</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km35-39ชาย" name="registerType">  35-39</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km40-44ชาย" name="registerType">  40 - 44</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km45-49ชาย" name="registerType">  45 - 49</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km50-54ชาย" name="registerType">  50 - 54</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km55-59ชาย" name="registerType">  55 - 59</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km60-64ชาย" name="registerType">  60 - 64</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km65-69ชาย" name="registerType">  65 - 69</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km70...ชาย" name="registerType">  70...</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="radio">
                                            <label><input type="radio" value="ไม่เกิน 15 ปีหญิง" name="registerType">  ไม่เกิน 15 ปี</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km16-19หญิง" name="registerType">  16 - 19</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km20-24หญิง" name="registerType">  20 - 24</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km25-29หญิง" name="registerType">  25 - 29</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km30-34หญิง" name="registerType">  30 - 34</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km35-39หญิง" name="registerType">  35-39</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km40-44หญิง" name="registerType">  40 - 44</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km45-49หญิง" name="registerType">  45 - 49</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km50-54หญิง" name="registerType">  50 - 54</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km55-59หญิง" name="registerType">  55 - 59</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="10.5km60...หญิง" name="registerType">  60...</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="radio">
                                            <label><input type="radio" value="5kmไม่เกิน 12 ปีชาย" name="registerType">  ไม่เกิน 12 ปี</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5kmไม่เกิน 15 ปีชาย" name="registerType">  ไม่เกิน 15 ปี</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km16-19ชาย" name="registerType">  16 - 19</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km20-29ชาย" name="registerType">  20 - 29</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km30-39ชาย" name="registerType">  30 - 39</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km40-49ชาย" name="registerType">  40 - 49</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km50-59ชาย" name="registerType">  50 - 59</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km60...ชาย" name="registerType">  60...</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="radio">
                                            <label><input type="radio" value="5kmไม่เกิน 12 ปีหญิง" name="registerType">  ไม่เกิน 12 ปี</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5kmไม่เกิน 15 ปีหญิง" name="registerType">  ไม่เกิน 15 ปี</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km16-19หญิง" name="registerType">  16 - 19</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km20-29หญิง" name="registerType">  20 - 29</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km30-39หญิง" name="registerType">  30 - 39</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km40-49หญิง" name="registerType">  40 - 49</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km50-59หญิง" name="registerType">  50 - 59</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" value="5km60...หญิง" name="registerType">  60...</label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                         <div class="radio">
                                            <label><input type="radio" value="ทีมครอบครัว (1,000 บาท)" name="registerType">  ทีมครอบครัว (1,000 บาท) 06.00 น.</label>
                                        </div>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-8 col-form-label">พ่อ แม่ ลูก , พ่อ ลูก ลูก , แม่ ลูก ลูก  ถ้วยรางวัล 1 - 5</label>
                                </div>
                                 <div class="form-group row">
                                   <div class="col-sm-4">
                                         <div class="radio">
                                            <label><input type="radio" value="เดิน - วิ่ง 3 k." name="registerType">  เดิน - วิ่ง 3 k. (350 บาท) 06.00 น.</label>
                                        </div>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-8 col-form-label">ไม่จำกัดเพศ และ อายุ</label>
                                </div>
                                 <div class="form-group row">
                                    <div class="col-sm-4">
                                         <div class="radio">
                                            <label><input type="radio" value="VIP" name="registerType">  VIP (1,000 บาท)</label>
                                        </div>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-8 col-form-label">ลงแข่งขันได้ทุกระยะ ได้รับถ้วยที่ระลึกทุกคน</label>
                                </div>
                                 <div class="form-group row">
                                   <div class="col-sm-4">
                                         <div class="radio">
                                            <label><input type="radio" value="แฟนซี" name="registerType">  แฟนซี</label>
                                        </div>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-8 col-form-label">ลงแข่งขันได้ทุกระยะ ได้รับถ้วยที่ระลึกทุกคน</label>
                                </div>  
                                 <div class="form-group row">
                                   <div class="col-sm-4">
                                         <div class="radio">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                    
                                </div>    
                            </form> 
                        </div>
                       
                    </div>
                  
                </div>
                
            </div>
        </div>	
    </section>
    <!-- End post-content Area -->
@endsection