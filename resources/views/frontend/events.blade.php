@extends('frontend.layout.master')

@section('content')

<!-- start banner Area -->
    		{{-- <section class="banner-area relative blog-home-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content blog-header-content col-lg-12">
							<h1 class="text-white">
								Dude You’re Getting
								a Telescope				
							</h1>	
							<p class="text-white">
								There is a moment in the life of any aspiring astronomer that it is time to buy that first
							</p>
							<a href="blog-single.html" class="primary-btn">View More</a>
						</div>	
					</div>
				</div>
			</section> --}}
			<!-- End banner Area -->				  

			<!-- Start top-category-widget Area -->
			{{-- <section class="top-category-widget-area pt-90 pb-90 ">
				<div class="container">
					<div class="row">		
						<div class="col-lg-4">
							<div class="single-cat-widget">
								<div class="content relative">
									<div class="overlay overlay-bg"></div>
								    <a href="#" target="_blank">
								      <div class="thumb">
								  		 <img class="content-image img-fluid d-block mx-auto" src="{{ url('/public/fitness')}}/img/blog/cat-widget1.jpg" alt="">
								  	  </div>
								      <div class="content-details">
								        <h4 class="content-title mx-auto text-uppercase">Social life</h4>
								        <span></span>								        
								        <p>Enjoy your social life together</p>
								      </div>
								    </a>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="single-cat-widget">
								<div class="content relative">
									<div class="overlay overlay-bg"></div>
								    <a href="#" target="_blank">
								      <div class="thumb">
								  		 <img class="content-image img-fluid d-block mx-auto" src="{{ url('/public/fitness')}}/img/blog/cat-widget2.jpg" alt="">
								  	  </div>
								      <div class="content-details">
								        <h4 class="content-title mx-auto text-uppercase">Politics</h4>
								        <span></span>								        
								        <p>Be a part of politics</p>
								      </div>
								    </a>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="single-cat-widget">
								<div class="content relative">
									<div class="overlay overlay-bg"></div>
								    <a href="#" target="_blank">
								      <div class="thumb">
								  		 <img class="content-image img-fluid d-block mx-auto" src="{{ url('/public/fitness')}}/img/blog/cat-widget3.jpg" alt="">
								  	  </div>
								      <div class="content-details">
								        <h4 class="content-title mx-auto text-uppercase">Food</h4>
								        <span></span>
								        <p>Let the food be finished</p>
								      </div>
								    </a>
								</div>
							</div>
						</div>												
					</div>
				</div>	
			</section> --}}
			<!-- End top-category-widget Area -->
			
			<!-- Start post-content Area -->
			<section class="post-content-area" style="padding-top:15%">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 posts-list">

							@foreach ($events as $event)
								<div class="single-post row" style="background-color:#ffff">
									<div class="col-lg-4  col-md-4 col-xs-12 col-sm-12 meta-details">
										<div class="user-details row" style="padding-left:15px; padding-right:15px">
											<a class="posts-title"  href="{{ url('/events/' . $event->id )}}"><h3>{{$event->title}}</h3></a>
											<p class="col-lg-12 col-md-12 col-6"><a href="{{ url('/events/' . $event->id )}}"><span>วันที่จัดงาน </span><span moment>{{ $event->to }}</span></a> <span class="lnr lnr-calendar-full"></span></p>
										</div>
									</div>
									<div class="col-lg-8 col-md-8 " style="margin-top:20px;margin-bottom:20px">
										<div class="feature-img ">
											<a href="{{ url('/events/' . $event->id )}}">
													<img class="img-fluid rounded" src="{{ url('/public/event_photo/' . $event->coverimage )  }}" alt="">
											</a>
										</div>
										{{-- <a class="posts-title" href="blog-single.html"><h3>{{$event->title}}</h3></a>
										<p class="excert">
											MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
										</p>
										<a href="blog-single.html" class="primary-btn">View More</a> --}}
									</div>
								</div>
							@endforeach

							

							
									
		                    <nav class="blog-pagination justify-content-center d-flex">
								{{ $events->links() }}
		                        
		                    </nav>
						</div>
					</div>
				</div>	
			</section>
			<!-- End post-content Area -->
			
@endsection	
	