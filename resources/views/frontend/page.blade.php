@extends('frontend.layout.master')
@section('content')
<section class="feature-area section-gap">
      <div class="container">
          <div class="row section-title">
              <h1>{{$page->title}}</h1>
              
          </div>						
          <div class="row justify-content-between align-items-center">
              <div class="col-lg-12">
                  <div class="single-feature">
                      {!! $page->description !!}
                  </div>								
              </div>
          </div>
      </div>	
  </section>
