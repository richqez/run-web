
@extends('frontend.layout.master')
@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
        <div class="overlay overlay-bg"></div>
        <div class="container">				
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                     {{ $event->title }}			
                    </h1>	
                </div>	
            </div>
        </div>
    </section>
    <!-- End banner Area -->					  
    
    <!-- Start post-content Area -->
    <section class="post-content-area single-post-area" style="padding-top:0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-12">
                            <div class="feature-img">
                                <img class="img-fluid" src="img/blog/feature-img1.jpg" alt="">

                            </div>									
                        </div>
                        </div>
                        
                        <div class="col-lg-12 col-md-12 TextEditorImg">
                            <div class="alert alert-success" role="alert">
                              <h4 class="alert-heading">ลงทะเบียนสำเร็จ</h4>
                              <p>เลขที่บัญชี ธ.กสิกรไทย 211 256 3205 หรือติดต่อกับเจ้าที่ได้ที่เบอร์ 081-290-9118</p>
                              <hr>
                            </div>
                        </div>
                         <div class="col-lg-12 col-md-12 TextEditorImg">
                            <a class="btn btn-primary" href="{{url('/')}}">หน้าหลัก</a>
                        </div>
                    </div>
                  
                </div>
                
            </div>
        </div>	
    </section>
    <!-- End post-content Area -->
@endsection