<footer class="footer-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-4  col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h4>ติดต่อเรา</h4>
                        <p>
                                35 หมู่ที่ 5 ตำบล นายาว อำเภอ พระพุทธบาท สระบุรี 18120
                        </p>
                        <p class="number">
                                081 290 9118
                        </p>
                    </div>
                </div>		
                <div class="col-md-4">
                    
                </div>				
              	<div class="col-md-4 col-md-offset-4 col-sm-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3857.84931351481!2d100.73783491484237!3d14.777516289689945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311dfd890428cf9b%3A0x1935abcf7718ddae!2sWat+Dong+Mani!5e0!3m2!1sen!2sth!4v1533399760558" style="width: 100% !important;height: 300px !important;" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>				
            </div>
            <!-- <div class="footer-bottom row align-items-center">
                <p class="footer-text m-0 col-lg-6 col-md-12">Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0.
Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0.</p>
             
            </div> -->
        </div>
    </footer>	