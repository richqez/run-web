<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <!-- Favicon-->
        <!-- link rel="shortcut icon" href="{{ url('/') }}/public/fitness/img/fav.png" -->
        <!-- Author Meta -->
        <!-- Meta Description -->
        <!-- Site Title -->
        <title>วัดดงมณี</title>

        <meta name="description" content="{{  isset($meta) && isset($meta->description) ? $meta->description : ''}}" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="{{ isset($meta) && isset($meta->keyword) ? $meta->keyword : ''}}" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
    
        <meta property="og:url" content="{{ isset($meta) && isset($meta->url) ? $meta->url : '' }}" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="{{ isset($meta) && isset($meta->title) ? $meta->title : '' }}" />
        <meta property="og:description" content="{{ isset($meta) && isset($meta->description) ? $meta->description : ''}}" />
        <meta property="og:image" content="{{ isset($meta) && isset($meta->img) ? $meta->img : '' }}" />

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans|Sunflower:300" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/linearicons.css">
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/bootstrap.css">
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/magnific-popup.css">
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/nice-select.css">							
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/animate.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/owl.carousel.css">			
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/jquery-ui.css">			
        <link rel="stylesheet" href="{{ url('/') }}/public/fitness/css/main.css">
    </head>
    <body>	
         
        @include('frontend.layout.header')

        @yield('content')
                                        
        @include('frontend.layout.footer')

        <script src="{{ url('/') }}/public/fitness/js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="{{ url('/') }}/public/fitness/js/vendor/bootstrap.min.js"></script>			
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="{{ url('/') }}/public/fitness/js/easing.min.js"></script>			
        <script src="{{ url('/') }}/public/fitness/js/hoverIntent.js"></script>
        <script src="{{ url('/') }}/public/fitness/js/superfish.min.js"></script>	
        <script src="{{ url('/') }}/public/fitness/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{ url('/') }}/public/fitness/js/jquery.magnific-popup.min.js"></script>	
        <script src="{{ url('/') }}/public/fitness/js/jquery-ui.js"></script>								
        <script src="{{ url('/') }}/public/fitness/js/jquery.nice-select.min.js"></script>	
        <script src="{{ url('/') }}/public/fitness/js/owl.carousel.min.js"></script>									
        <script src="{{ url('/') }}/public/fitness/js/mail-script.js"></script>	
        <script src="{{ url('/') }}/public/fitness/js/main.js"></script>	
        <script src="{{ url('/') }}/public/js/moment-with-locales.min.js"></script>	
        <script>    
                moment.locale('th');
                $('span[moment]').each((i,e) => {
                    let x = $(e).text();
                    $(e).text( moment(x, "YYYY-MM-DD").format('ll') + '  ' + moment(x, "YYYY-MM-DD").fromNow())
                })
            </script>
    
    </body>
</html>