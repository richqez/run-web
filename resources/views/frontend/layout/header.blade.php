<header id="header">
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-4 col-4 header-top-left no-padding">
                    <!-- <a href="mailto:support@colorlib.com"><span class="lnr lnr-location"></span></a> -->
                    <!-- <a class="contact-texts" href="mailto:support@colorlib.com"></a>    -->
                    <h3 class="contact-texts">วัดดงมณี</h3>     
                </div>
                <div class="col-md-4 col-4 header-top-bottom no-padding">
                    <a href="index.html"><img class="newLogo" src="http://dongmanee.com/intro_center.jpg" alt="" title=""></a>

                        
                </div>
                <div class="col-md-4 col-4 header-top-right no-padding">
                    <h3 class="contact-texts">น้ำดื่ม ศาลาแพร่</h3>     
                   <!--  <a class="contact-texts" href="tel:+440 123 12 658 439">ศาลาแพร่</a> -->
                   <!--  <a href="tel:+440 123 12 658 439"><span class="lnr lnr-phone-handset"></span></a> -->
                </div>                                                      
            </div>                              
        </div>
    </div>
    <div class="container-fluid main-menu">
        <div class="row align-items-center justify-content-center">	
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="{{url('/')}}">หน้าหลัก</a></li>
                    <li><a href="{{url('/events')}}">ข่าวและกิจกรรม</a></li>
                    @php
                        function render_menu($menu) { 
                            $isHasChildren = isset($menu->children) && sizeof($menu->children) > 0;
                            if ($isHasChildren) {
                                $html = '<li class="menu-has-children">' ;
                            }else {
                                $html = '<li>' ;
                            }
                            $html .= '<a href="' . url('/view/'. $menu->pagecode ) . '">' . $menu->name . '</a>';
                            if ($isHasChildren) {
                               $html .= '<ul>';
                               foreach ($menu->children as $v) {
                                $html .= render_menu($v);
                               }
                               $html .= '</ul>';
                            }
                            $html .= '</li>' ;
                            return $html ; 
                        } 
                    @endphp
                    @foreach($app_menus as $m)
                        {!! render_menu($m) !!}
                    @endforeach

                    {{-- <li class="menu-has-children">
                        <a href="">Blog</a>
                        <ul>
                          <li><a href="blog-home.html">Blog Home</a></li>
                          <li><a href="blog-single.html">Blog Single</a></li>
                          <li class="menu-has-children"><a href="">Level 2</a>
                            <ul>
                              <li><a href="#">Item One</a></li>
                              <li><a href="#">Item Two</a></li>
                               <li class="menu-has-children">
                                   <a href="">Level 2</a>
                                <ul>
                                  <li><a href="#">Item One</a></li>
                                  <li><a href="#">Item Two</a></li>
                                </ul>
                              </li>   
                            </ul>
                          </li>                               
                        </ul>
                    </li> --}}
                </ul>
            </nav><!-- #nav-menu-container -->		
        </div>
    </div>
</header><!-- #header -->