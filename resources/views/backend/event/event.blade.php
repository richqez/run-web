@extends('backend.layout.master') 
@section('content')
<!-- BEGIN CONTENT BODY -->


<div class="page-content">
    <!-- END PAGE BREADCRUMB -->
    <div class="portlet light bordered" style="margin-bottom:10px">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">
                    สร้างกิจกรรม
                </span>
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" action="{{ url('/backend/events/save') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">ชื่อกิจกรรม</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ isset($event) ?  $event->title : '' }}" class="form-control" name="title">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">วันที่เริ่ม</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ isset($event) ?  $event->from : '' }}" class="form-control" name="from">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">วันที่สิ้นสุด</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ isset($event) ?  $event->to : '' }}" class="form-control" name="to">
                            </div>
                        </div>




                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                <textarea name="description">
                                    {!! isset($event) ? $event->description : '' !!}
                                </textarea>
                            </div>
                        </div>

                        @if(isset($event) && $event->coverimage != '')
                        <div class="form-group">
                            <label for="show" class="col-sm-2 control-label">       
                            </label>
                            <div class="co-sm-10">
                                <img class="img-responsive" width="200" height="200" src="{{ url('/public/event_photo/'.$event->coverimage) }}" />
                            </div>

                        </div>
                        @endif

                        <div class="form-group">
                            <label for="event_photo" class="col-sm-2 control-label">รูปปก</label>
                            <div class="col-sm-10">
                                <input type="file" name="event_photo">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="submit" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-green" value="submit">
                            </div>
                        </div>


                        <input type="hidden" name="id" class="btn btn-green" value="{{ isset($event) ? $event->id  : ''}}">

                    </form>
                </div>
            </div>

        </div>
    </div>

    <input name="image" type="file" id="upload" class="hidden" onchange="">
</div>
<!-- END CONTENT BODY -->
@endsection
 
@section('custom_script')
<script>
    tinymce.init({
    height:360,
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    convert_urls : false,
    selector: "textarea",
    theme: "modern",
    paste_data_images: true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | sizeselect | bold italic | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image",
    toolbar2: "print preview  | forecolor backcolor emoticons",
    image_advtab: true,
    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
         
          let file = this.files[0];
            
            console.log(file[0])

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
                });

          let formData = new FormData();
          formData.append("event_photo_content",file);


          $.ajax({
            url: "{{  url('/backend/events/uploadImgContent') }}",
            type: 'POST',
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                callback(response.data , { alt: ''});
            }
        });

        });


      }
    },
    templates: [{
      title: 'Test template 1',
      content: 'Test 1'
    }, {
      title: 'Test template 2',
      content: 'Test 2'
    }]
  });


  $('input[name=to],input[name=from]').datepicker({ 
        language: 'th',
        format: 'yyyy-mm-dd'
        
    });

</script>
@endsection