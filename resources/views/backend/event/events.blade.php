@extends('backend.layout.master') 
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- END PAGE BREADCRUMB -->
    <div class="portlet light bordered" style="margin-bottom:10px">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">
                    รายการกิจกรรม
                </span>
            </div>
        </div>

        <div class="portlet-body">
            <a href="{{ url('/backend/events/create') }}" class="btn btn-primary btn-md" role="button">สร้าง</a>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th>ชื่อกิจกรรม</th>
                    <th>วันที่</th>
                    <th></th>
                </tr>
                @foreach ($events as $event)
                <tr>
                    <td>{{ $event->title }}</td>
                    <td>
                        <span formatdate>{{ $event->from }}</span> -
                        <span formatdate>{{ $event->to }}</span>
                    </td>
                    <td>
                        <a href="{{ url('/backend/events/eventRegister/'.$event->id) }}" class="btn btn-primary btn-md" role="button">รายชื่อการลงทะเบียน</a>
                        <a href="{{ url('/backend/events/edit/'.$event->id) }}" class="btn btn-primary btn-md" role="button">แก้ไข</a>
                        <a href="{{ url('/backend/events/delete/'.$event->id) }}" class="btn btn-danger btn-md" role="button">ลบ</a>
                    </td>
                </tr>
                @endforeach
            </table>
            {{ $events->links() }}
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection