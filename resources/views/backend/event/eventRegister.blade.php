@extends('backend.layout.master') 
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- END PAGE BREADCRUMB -->
    <div class="portlet light bordered" style="margin-bottom:10px">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">
                    รายชื่อการลงทะเบียน :  {{ $event->title }}
                </span>
            </div>
        </div>

        <div class="portlet-body">
            <br>
            <a class="btn btn-primary" target="_blank" href="{{ url('/backend/events/eventRegister/'.$event->id . '/export') }}">ส่งออก</a>
            <table class="table table-bordered">
                <tr>
                    <th>ชื่อ</th>
                    <th>นามสกุล</th>
                    <th>ชมรม</th>
                    <th>ที่อยู่</th>
                    <th>รหัสไปรษณืย์</th>
                    <th>เบอร์โทรศัพท์</th>
                    <th>ไซส์</th>
                    <th>BIB</th>
                    <th>ประเภท</th>
                </tr>
                @foreach ($application as $data)
                <tr>
                    <td>{{ $data->firstName }}</td>
                    <td>
                        {{ $data->lastName }}
                    </td>
                    <td>{{ $data->clubName }}</td>
                    <td>{{ $data->address }}</td>
                    <td>{{ $data->zipCode }}</td>
                    <td>{{ $data->tel }}</td>
                    <td>{{ $data->size }}</td>
                    <td>{{ $data->bib }}</td>
                    <td>{{ $data->registerType }}</td>
                </tr>
                @endforeach
            </table>
            {{ $application->links() }}
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection