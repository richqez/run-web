@extends('backend.layout.master') 
@section('content')
<!-- BEGIN CONTENT BODY -->


<div class="page-content">
    <!-- END PAGE BREADCRUMB -->

  


    <div class="portlet light bordered" style="margin-bottom:10px">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">
                    จัดการเมนู
                </span>
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" action="{{ url('/backend/menus/save') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">ชื่อ</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ isset($menu) ?  $menu->name : '' }}" class="form-control" name="name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">รหัสหน้า</label>
                            <div class="col-sm-10">
                                {{-- <input type="text" value="{{ isset($menu) ?  $menu->pagecode : '' }}" class="form-control" name="pagecode"> --}}
                                <select class="form-control" name="pagecode">
                                    @foreach($pages as $p)
                                        <option value="{{$p->code}}">{{$p->title}}  ({{$p->code}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="submit" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-green" value="submit">
                            </div>
                        </div>


                        <input type="hidden" name="id" class="btn btn-green" value="{{ isset($menu) ? $menu->id  : ''}}">

                    </form>
                </div>
            </div>

        </div>
    </div>

    <input name="image" type="file" id="upload" class="hidden" onchange="">
</div>
<!-- END CONTENT BODY -->
@endsection


