@extends('backend.layout.master')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>หน้าจัดการกิจกรรม
                <small>blank page layout</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Hello</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Blank Page</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Page Layouts</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="note note-info">
        <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
    </div>
    <!-- END PAGE BASE CONTENT -->
    <div class="col-md-12">
        @foreach ($events as $event)
            <p>This is event {{ $event->title }}</p>
            {!! $event->description !!}
        @endforeach
     
    </div>
</div>
<!-- END CONTENT BODY -->
@endsection

@section('custom_script')
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection