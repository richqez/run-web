@extends('backend.layout.master') 
@section('content')
<!-- BEGIN CONTENT BODY -->


<div class="page-content">
    <!-- END PAGE BREADCRUMB -->
    <div class="portlet light bordered" style="margin-bottom:10px">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">
                    สร้างกิจกรรม
                </span>
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" action="{{ url('/backend/pages/save') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">ชื่อหน้า</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ isset($page) ?  $page->title : '' }}" class="form-control" name="title">
                            </div>
                        </div>

                        <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">รหัสหน้า</label>
                                <div class="col-sm-10">
                                    <input {{ isset($page) ? 'disabled' : ''}}  type="text" value="{{ isset($page) ?  $page->code : '' }}" class="form-control" name="code">
                                </div>
                            </div>

                        <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">คำค้น</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{ isset($page) ?  $page->keyword : '' }}" class="form-control" name="keyword">
                                </div>
                        </div>

                     


                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">เนื้อหา</label>
                            <div class="col-sm-10">
                                <textarea name="description">
                                    {!! isset($page) ? $page->description : '' !!}
                                </textarea>
                            </div>
                        </div>

                        
   

                        <div class="form-group">
                            <label for="submit" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-green" value="submit">
                            </div>
                        </div>


                        <input type="hidden" name="id" class="btn btn-green" value="{{ isset($page) ? $page->id  : ''}}">

                    </form>
                </div>
            </div>

        </div>
    </div>

    <input name="image" type="file" id="upload" class="hidden" onchange="">
</div>
<!-- END CONTENT BODY -->
@endsection
 
@section('custom_script')
<script>
    tinymce.init({
    height:480,
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    convert_urls : false,
    selector: "textarea",
    theme: "modern",
    paste_data_images: true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | sizeselect | bold italic | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image",
    toolbar2: "print preview  | forecolor backcolor emoticons",
    image_advtab: true,
    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
         
          let file = this.files[0];
            
            console.log(file[0])

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
                });

          let formData = new FormData();
          formData.append("event_photo_content",file);


          $.ajax({
            url: "{{  url('/backend/pages/uploadImgContent') }}",
            type: 'POST',
            data: formData,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                callback(response.data , { alt: ''});
            }
        });

        });


      }
    },
    templates: [{
      title: 'Test template 1',
      content: 'Test 1'
    }, {
      title: 'Test template 2',
      content: 'Test 2'
    }]
  });


  $('input[name=to],input[name=from]').datepicker({ 
        language: 'th',
        format: 'yyyy-mm-dd'
        
    });

</script>
@endsection