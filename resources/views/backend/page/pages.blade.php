@extends('backend.layout.master') 
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- END PAGE BREADCRUMB -->
    <div class="portlet light bordered" style="margin-bottom:10px">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">
                    รายการหน้า
                </span>
            </div>
        </div>

        <div class="portlet-body">
            <a href="{{ url('/backend/pages/create') }}" class="btn btn-primary btn-md" role="button">สร้าง</a>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th>ชื่อหน้า</th>
                    <th>รหัสหน้า</th>
                    <td>ที่อยู่หน้า</td>
                    <th></th>
                </tr>
                @foreach ($pages as $page)
                <tr>
                    <td>{{ $page->title }}</td>
                    <td>
                        <span>{{ $page->code }}</span>
                    </td>
                    <td>
                        <a href="{{ url('/view/'.$page->code) }}" >{{ url('/view/'.$page->code) }}</a>
                    </td>
                    <td>
                        <a href="{{ url('/backend/pages/edit/'.$page->id) }}" class="btn btn-primary btn-md" role="button">แก้ไข</a>
                        <a href="{{ url('/backend/pages/delete/'.$page->id) }}" class="btn btn-danger btn-md" role="button">ลบ</a>
                    </td>
                </tr>
                @endforeach
            </table>
            {{ $pages->links() }}
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection