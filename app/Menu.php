<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    protected $with = ['children'];


    public function children() {
        return $this->hasMany(Menu::class ,'parentId', 'id');
    }
}
