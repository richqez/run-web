<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

use Intervention\Image\ImageManagerStatic as Image;



class PageController extends Controller
{
    
    public function index(){
        $x = Page::orderBy('created_at', 'desc')->paginate(5);
        return view('backend.page.pages')
                ->with('pages',$x);
    }

    public function create(){
        return view('backend.page.page');
    }

    public function edit($id){
        $x = Page::find($id);
        return view('backend.page.page')
                ->with('page',$x);
    }

    public function delete($id){
        $event = Page::find($id);
        $event->forceDelete();
        return redirect('backend/pages');
    }

    public function uploadImgContent(Request $request){
        if($request->event_photo_content){
            $savePath = public_path('page_photo_content') . "/" . date('m-d-Y') . "/" ;
            $photoName = time() . '.' . $request->event_photo_content->getClientOriginalExtension();
            $fileName =  $savePath  . $photoName;
            if (!file_exists($savePath)) {
                mkdir($savePath);
            }
            $img = Image::make($request->event_photo_content->getRealPath())->resize(800, 600);
            $img->save($fileName);
            return ["data" => url( "/public/page_photo_content/" . date('m-d-Y') . "/" .$photoName) ];
        }
        return $request->all();
    }

    
    public function save(Request $request){


        $page = new Page;

        if ($request->id != '') {
            $page =  Page::find($request->id);
        }else{
            $existPageCode = Page::where('code',$request->code)->first();
            if (isset($existPageCode)) {
                $request->code  = $request->code . time();
            }
        }

        


        $page->title = $request->title;
        $page->description = $request->description;
        $page->code = $request->code ;
        $page->keyword = $request->keyword;

        $page->save();

        return redirect('backend/sitepage');
    }




}
