<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Event;
use App\Application;
use Intervention\Image\ImageManagerStatic as Image;

class EventController extends Controller
{
    public function __invoke(Request $request)
    {
        //
    }

    public function index(){
        $events = Event::orderBy('created_at', 'desc')->paginate(5);
        return view('backend.event.events')
                ->with('events',$events);
    }

    public function eventRegister($refEventID){
        $event = Event::find($refEventID);
        $application = Application::orderBy('created_at', 'desc')->where('refEventID',$refEventID)->paginate(100);
        return view('backend.event.eventRegister')
                ->with('application',$application)
                ->with('event',$event);;
    }

    public function eventRegisterExport($refEventID){
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Encoding'     => 'UTF-8',
                'Content-Transfer-Encoding' => 'binary'
            ,   'Content-type'        => 'text/csv;charset=UTF-8'
            ,   'Content-Disposition' => 'attachment; filename=export-' . time() . '.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        $list = Application::orderBy('created_at', 'desc')->where('refEventID',$refEventID)->get()->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

       $callback = function() use ($list) 
        {
            $FH = fopen('php://output', 'w');
            fprintf($FH, chr(0xEF).chr(0xBB).chr(0xBF));

            foreach ($list as $row) { 
               
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers); //use Illuminate\Support\Facades\Response;
    }

    public function create(){
        return view('backend.event.event');
    }


    public function edit($id){
        $event = Event::find($id);
        return view('backend.event.event')
                ->with('event',$event);
    }

    public function delete($id){
        $event = Event::find($id);
        $event->forceDelete();
        return redirect('backend/events');
    }


    public function uploadImgContent(Request $request){
        if($request->event_photo_content){

            $savePath = public_path('event_photo_content') . "/" . date('m-d-Y') . "/" ;
            $photoName = time() . '.' . $request->event_photo_content->getClientOriginalExtension();
            $fileName =  $savePath  . $photoName;

            if (!file_exists($savePath)) {
                mkdir($savePath);
            }
            $img = Image::make($request->event_photo_content->getRealPath())->resize(800, 600);
            $img->save($fileName);

            return ["data" => url( "/public/event_photo_content/" . date('m-d-Y') . "/" . $photoName) ];
        }
        return $request->all();
    }

    public function save(Request $request){


        $event = new Event;

        if ($request->id != '') {
            $event =  Event::find($request->id);
        }

        $event->title = $request->title;
        $event->description = $request->description;

       
        $event->from = $request->from;
        $event->to = $request->to;
        $event->active = true;

        if($request->event_photo){
            $photoName = time() . '.' . $request->event_photo->getClientOriginalExtension();
            $event->coverimage = $photoName;
            $img = Image::make($request->event_photo->getRealPath())->resize(800, 600);
            $img->save(public_path('event_photo') . "/" . $photoName);
        }

        $event->save();
        return redirect('backend/events');
    }






}
