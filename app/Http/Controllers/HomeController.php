<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Event;
use App\Page;


class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function __construct()
    {
       // $this->middleware('auth');
    }
    

    public function index(){

        $defaultSizrOfCol = 3 ;

        $events = Event::orderBy('created_at', 'desc')->paginate(5);
        

        $allfiles = array_diff(scandir(public_path() . '/' . 'home_images'), array('.', '..'));

        $files = array_map(function($item){
            return url('/public/home_images/'. $item);
        },$allfiles);

 

        if($defaultSizrOfCol > sizeof($files)){
            $ch = array_chunk($files,1,true);
        }else{
            $x =  floor(sizeof($files) / $defaultSizrOfCol);
            $x =  ($defaultSizrOfCol % sizeof($files))  == 0  ? $x + 0 : $x + 1;
            $ch = array_chunk($files, $x ,true);
        }

        return view('frontend.home')
                ->with('events',$events)
                ->with('home_image', $ch)
                ->with('meta', (object)[
                    "keyword" => "",
                    "description" => "",
                    "url" => "",
                    "title" => "DMT-HALF-MARATHON",
                    "img"=>""
                    
                ]);
    }

    public function viewpage($codepage){

        $page = Page::where('code', $codepage)->first();

        if ($page) {
            return view('frontend.page')
                    ->with('page',$page)
                    ->with('meta', (object)[
                        "keyword" => $page->keyword,
                        "description" => $page->title,
                        "url" => "",
                        "title" => $page->title,
                        "img"=>""
                    ]);
    
        }

        return abort(404);
        
    }

    public function events(){
        $events = Event::orderBy('created_at', 'desc')->paginate(5);
        return view('frontend.events')
                ->with('events',$events);
    }

    public function event($id){

        $event = Event::find($id);
        return view('frontend.event')
                ->with('event',$event)
                ->with('meta', (object)[
                    "keyword" => $event->title,
                    "description" => $event->title,
                    "url" => url('/events') . "/" .$id ,
                    "title" => $event->title,
                    "img"=> url('public/event_photo') . "/" . $event->coverimage
                ]);

    }


    public function blankpage(){

        return view('backend.admin')->with('events',Event::all());
    }

  

}
