<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Event;
use App\Page;
use App\Application;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
  
    public function index($id)
    {

        $event  = Event::find($id);

         return view('frontend.register')->with('event',$event);
    }

    public function result($id)
    {

        $event  = Event::find($id);

        return view('frontend.registerResult')->with('event',$event);
    }

    public function save(Request $request){


        $application = new Application;

        $application->firstName = $request->firstName;
        $application->lastName = $request->lastName;      
        $application->clubName = $request->clubName;
        $application->address = $request->address;
        $application->zipCode = $request->zipCode;
        $application->tel = $request->tel;
        $application->size = $request->size;
        $application->bib = $request->bib;
        $application->registerType = $request->registerType;
        $application->bib = $request->bib;
        $application->refEventID = $request->refEventID;
        $application->save();

        return $this->result($application->refEventID);
        
    }
}
