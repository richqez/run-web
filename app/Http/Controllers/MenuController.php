<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Page;

class MenuController extends Controller
{
    public function index(){
        $x = Menu::orderBy('seq', 'asc')->paginate(5);
        $xx = Menu::whereNull('parentId')->orderBy('seq', 'asc')->get();
        return view('backend.menu.menus')
                ->with('menus',$x)
                ->with('menus_null',$xx);
    }

    public function create(){

        return view('backend.menu.menu')
                ->with('pages', Page::All());
    }

    public function edit($id){
        $x = Menu::find($id);
        return view('backend.menu.menu')
                ->with('pages', Page::All())
                ->with('menu',$x);
    }

    public function delete($id){
        $event = Menu::find($id);

        $chhildren = Menu::where('parentId',$id)->get();
        foreach ($chhildren as $c) {
            $c->parentId = null;
            $c->save();
        }

        $event->forceDelete();
        return redirect('backend/menus');
    }

    public function updateposition(Request $request){
        $x = json_decode($request->getContent(),true);
        $this->updateposition_recusive($x);
        return ["data" => "ok"];
    }

    public function updateposition_recusive($menus, $parentId = null ){
        $idx = 1 ;
        foreach ($menus as $value) {
            $updateItem =  Menu::find($value['id']);
            $updateItem->parentId = $parentId;
            $updateItem->seq = $idx;
            $updateItem->save();
            $idx++;
            if (isset($value['children'])) {
                $this->updateposition_recusive($value['children'],$value['id']);
            }
        }
    }

    public function save(Request $request){


        $menu = new Menu;

        if ($request->id != '') {
            $menu =  Menu::find($request->id);
        }

        $menu->pagecode = $request->pagecode;
        $menu->name = $request->name;

        $menu->save();

        return redirect('backend/menus');
    }



}
